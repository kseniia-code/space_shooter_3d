uniform sampler2D tex1, tex3;
varying vec2 TexCoord;

uniform float BlendFactor;

void main()
{

	vec2 TexCoord2 = TexCoord;
	
	vec4 texel0 = texture2D(tex1, TexCoord);
    vec4 texel1 = texture2D(tex3, TexCoord2);
	
	gl_FragColor = mix(texel0, texel1, BlendFactor);
	//gl_FragColor = texture2D(tex1, TexCoord);

}