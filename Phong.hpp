#pragma once

#include <windows.h>
#include <GL/glew.h>
#include <GL/glut.h>
#include "SceneObject.hpp"
#include "Shader.hpp"

#include <GL/gl.h>
#include <GL/glu.h>


class Phong : public SceneObject
{
public:
    Phong();
    ~Phong();

    glm::vec3 getPos();    


protected:
    void privateInit();
    void privateRender();
    void privateUpdate();

private:
    glm::vec3 pos_;
    float radius;
    GLuint list_id;
    Shader s;
    GLint light;

};

