uniform samplerCube skybox;
varying vec4 TexCoord;

void main()
{
	vec3 cube = vec3 (textureCube(skybox, TexCoord.xyz));
	gl_FragColor = vec4 (cube, 0.0);

}