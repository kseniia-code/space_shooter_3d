varying vec4 TexCoord;

void main(void)
{		
    TexCoord      = gl_Vertex;
    gl_Position	  = gl_ModelViewProjectionMatrix * gl_Vertex;
	
}