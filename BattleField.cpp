#include "BattleField.hpp"
#include "glm/ext.hpp"
#include"glm/gtx/string_cast.hpp"
#include <iostream>

#include <SOIL.h>

BattleField::BattleField(int type)
{
    t_ = type;

}

BattleField::~BattleField()
{
}

void BattleField::privateInit()
{

    s.initShaders("./shaders/field");

    //size of field
    int width = 64;
    int height = 256;

    //colormap for field
    //unsigned char* image = SOIL_load_image("./images/colormap.bmp", &width, &height, 0, SOIL_LOAD_RGB);
    unsigned char* image1 = SOIL_load_image("./images/colorMap2012.bmp", &width, &height, 0, SOIL_LOAD_RGB);

    // Generate texture name
    glGenTextures(1, &tex1);

    // Create texture object
    glBindTexture(GL_TEXTURE_2D, tex1);

    // Set texture parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    // Set environment mode
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    // Specify texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA_FLOAT32_ATI, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image1);

    //hightmap for field
    unsigned char* image2 = SOIL_load_image("./images/heightMap2012.bmp", &width, &height, 0, SOIL_LOAD_RGB);

    // Generate texture name
    glGenTextures(1, &tex2);

    // Create texture object
    glBindTexture(GL_TEXTURE_2D, tex2);

    // Set texture parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    // Set environment mode
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    // Specify texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA_FLOAT32_ATI, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image2);

    //lightmap for field
    unsigned char* image3 = SOIL_load_image("./images/lightMap2012.bmp", &width, &height, 0, SOIL_LOAD_RGB);

    // Generate texture name
    glGenTextures(1, &tex3);

    // Create texture object
    glBindTexture(GL_TEXTURE_2D, tex3);

    // Set texture parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    // Set environment mode
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    // Specify texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA_FLOAT32_ATI, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image3);

    glPrimitiveRestartIndex(32768);

    glm::vec3 v;
    glm::vec2 t;
    int x_size = 64;
    int z_size = 512;

    //create field grid

//    for(int x = -320 ; x < 320 ; x += 10){
//        for(int z = 0 ; z > -5120 ; z -= 10){
    for(int x = 0 ; x < x_size ; x += 1){
        for(int z = 0 ; z < z_size ; z += 1){

            // create vertex arrays
            v[0]= x*10.0f - 320.0f;
            v[1]= 0;
            v[2]= z*(-10.0f) - pos_;
            pos_z = v[2];
            vertexArray_.push_back(v);

            // create grid
            t[0] = x/float(x_size-1);
            t[1] = z/float(z_size-1);

            textureArray_.push_back(t);

        }
    }

            for(int x = 0 ; x < x_size - 1; x += 1){
                for(int z = 0 ; z < z_size ; z += 1){

                indexArray_.push_back(z+z_size*x);
                indexArray_.push_back(z+z_size*x+z_size);
                }

                indexArray_.push_back(32768);
            }


    indexArray_.pop_back();

    s.enable();

    texture_ = glGetUniformLocation(s.getProg(), "tex1");
    glUniform1i(texture_, 0);

    texture_ = glGetUniformLocation(s.getProg(), "tex2");
    glUniform1i(texture_, 1);

    texture_ = glGetUniformLocation(s.getProg(), "tex3");
    glUniform1i(texture_, 2);

    blend_ = glGetUniformLocation(s.getProg(), "BlendFactor");
    glUniform1f(blend_, 0.3);

    time_ = glGetUniformLocation(s.getProg(), "time");
    glUniform1f(time_, 5.0);

    s.disable();

}

void BattleField::privateRender()
{

    s.enable();

    //different types of battlefield we will move at different times
    if (t%2000 == 0){
    if (t_ == 0) t = 0;
    }

    if (t%4000 == 0){
    if (t_ == 1) t = 0;
    }

    t += 5.0;

    glUniform1f(time_, t);


    glEnable (GL_PRIMITIVE_RESTART);
    //glPolygonMode(GL_FRONT, GL_LINE);

    glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tex1);

    glActiveTexture(GL_TEXTURE1);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tex2);

    glActiveTexture(GL_TEXTURE2);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tex3);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glColor3f(1.0f, 1.0f, 1.0f);
    glTexCoordPointer(2, GL_FLOAT, 0, &textureArray_[0]);

    glVertexPointer(3, GL_FLOAT, 0, &vertexArray_[0]);
    //glDrawArrays (GL_POINTS, 0, vertexArray_.size());
    glDrawElements(GL_TRIANGLE_STRIP, indexArray_.size(), GL_UNSIGNED_INT, &indexArray_[0]);

    glDisableClientState(GL_VERTEX_ARRAY);

    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisable (GL_PRIMITIVE_RESTART);

    glActiveTexture(GL_TEXTURE2);
    glDisable(GL_TEXTURE_2D);

    glActiveTexture(GL_TEXTURE1);
    glDisable(GL_TEXTURE_2D);

    glActiveTexture(GL_TEXTURE0);
    glDisable(GL_TEXTURE_2D);

    s.disable();
}

void BattleField::privateUpdate()
{

}

void BattleField::setPos(int pos){
    pos_ = pos;
}

float BattleField::getPos_z(){
    return pos_z;
}

