
#include "Phong.hpp"

#include <iostream>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include <SOIL.h>


Phong::Phong()
{
    pos_ = glm::vec3(0.0f, 0.0f, 0.0f);
    radius = 3.0f;
}

Phong::~Phong()
{
}

void Phong::privateInit()
{
    s.initShaders("./shaders/phong");

    list_id = glGenLists(1);
    glNewList(list_id, GL_COMPILE);
    glTranslatef(0.0, 500.0, -500.0);
    glutSolidSphere(30,50,50);

    glEndList();

}


void Phong::privateRender()
{
    s.enable();

    glEnable(GL_LIGHTING);
    light = glGetUniformLocation(s.getProg(), "lightVec");
    glUniform3f(light, 100.0, 200.0, 200.0);

    glCallList(list_id);
    glDisable(GL_LIGHTING);

    s.disable();

}


void Phong::privateUpdate()
{


}

glm::vec3 Phong::getPos(){

    return pos_;
}



