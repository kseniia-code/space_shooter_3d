varying vec3 Normal;
uniform vec3 lightVec;

void main () 
{
	
	vec3 light = normalize(lightVec);
	
	const vec3 diffuseMaterial = vec3(0.0, 1.0, 0.0);
	const vec3 specularMaterial = vec3(1.0, 1.0, 1.0);
    //const vec3 diffuseMaterial = gl_FrontLightProduct[0].diffuse.xyz;
    //const vec3 specularMaterial = gl_FrontLightProduct[0].specular.xyz;
	
    vec3 NormalVec = normalize(Normal);
    // calculate half angle vector
    vec3 eyeVec = vec3(0.0, 0.0, 1.0);
    vec3 halfVec = normalize(light + eyeVec);

    // calculate diffuse component
    vec3 diffuse = vec3(max(0.5*dot(NormalVec, light), 0.5)) * diffuseMaterial;

    // calculate specular component
    vec3 specular = vec3(max(dot(NormalVec, halfVec), 0.0));
    specular = pow(specular.x, 32.0) * specularMaterial;

    // combine diffuse and specular contributions and output final vertex color
    gl_FragColor.rgb = diffuse + specular;
}
