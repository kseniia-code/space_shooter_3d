#pragma once

#include <stdlib.h>
#include <windows.h>
#include <GL/glew.h>
#include <GL/glut.h>
#include "SceneObject.hpp"
#include "Shader.hpp"
#include "Camera.hpp"

#include <GL/gl.h>
#include <GL/glu.h>


class SkyBox : public SceneObject
{
public:
    SkyBox();
    ~SkyBox();

    void moveRight();
    void moveLeft();
    glm::vec3 getPos();
    void setPos(const glm::vec3 &pos);
    GLuint getSkyTex();

protected:
    void privateInit();
    void privateRender();
    void privateUpdate();

private:
    glm::vec3 pos_;
    GLuint list_id;
    GLuint skybox;
    Shader s;
    GLint texLoc;
    std::shared_ptr<Camera> cam_;

};

