uniform sampler2D tex2;
varying vec2  TexCoord;
uniform float time;


void main(void)
{	
    TexCoord      = gl_MultiTexCoord0.xy;
	vec4 position = gl_Vertex;
	
	
	position.y = (texture2D(tex2, TexCoord.xy).x * 200.0) - 200.0;	
	position.x *= 2.0;

	
	position.z += time;
		
    gl_Position	  = gl_ModelViewProjectionMatrix * position;
	
}