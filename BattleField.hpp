#pragma once

#include <windows.h>
#include "Shader.hpp"
#include <GL/glew.h>
#include "GL/freeglut.h"
#include <GL/gl.h>
#include <GL/glu.h>

#include "SceneObject.hpp"
#include "glm/glm.hpp"

class BattleField : public SceneObject
{
public:
    BattleField(int type);
    ~BattleField();
     void setPos(int pos);
     float getPos_z();

protected:
    virtual void privateInit();
    virtual void privateRender();
    virtual void privateUpdate();

private:
    std::vector< glm::vec3 > vertexArray_;
    std::vector< unsigned int > indexArray_;
    std::vector< glm::vec2 > textureArray_;
    GLuint tex1, tex2, tex3;
    GLint texture_, time_, blend_;
    Shader s;
    int t, t_, pos_;
    float pos_z;
};

