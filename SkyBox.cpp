#include "SkyBox.hpp"

#include <iostream>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include <SOIL.h>


SkyBox::SkyBox()
{
    //pos_ = glm::vec3(0.0f, 0.0f, -50.0f);
}

SkyBox::~SkyBox()
{
}

void SkyBox::privateInit()
{

    s.initShaders("./shaders/skybox");

    int width = 512;
    int height = 512;

    //pictures for skybox
//    unsigned char* image1 = SOIL_load_image("./images/colorMap2012_1.bmp", &width, &height, 0, SOIL_LOAD_RGB);
//    unsigned char* image2 = SOIL_load_image("./images/colorMap2012_1.bmp", &width, &height, 0, SOIL_LOAD_RGB);
//    unsigned char* image3 = SOIL_load_image("./images/colorMap2012_1.bmp", &width, &height, 0, SOIL_LOAD_RGB);
//    unsigned char* image4 = SOIL_load_image("./images/colorMap2012_1.bmp", &width, &height, 0, SOIL_LOAD_RGB);
//    unsigned char* image5 = SOIL_load_image("./images/skybox_up.bmp", &width, &height, 0, SOIL_LOAD_RGB);
//    unsigned char* image6 = SOIL_load_image("./images/colorMap2012_1.bmp", &width, &height, 0, SOIL_LOAD_RGB);

    unsigned char* image1 = SOIL_load_image("./images/skybox_down.bmp", &width, &height, 0, SOIL_LOAD_RGB);
    unsigned char* image2 = SOIL_load_image("./images/skybox_east.bmp", &width, &height, 0, SOIL_LOAD_RGB);
    unsigned char* image3 = SOIL_load_image("./images/skybox_north.bmp", &width, &height, 0, SOIL_LOAD_RGB);
    unsigned char* image4 = SOIL_load_image("./images/skybox_south.bmp", &width, &height, 0, SOIL_LOAD_RGB);
    unsigned char* image5 = SOIL_load_image("./images/skybox_up.bmp", &width, &height, 0, SOIL_LOAD_RGB);
    unsigned char* image6 = SOIL_load_image("./images/skybox_west.bmp", &width, &height, 0, SOIL_LOAD_RGB);

    // Generate texture name
    glGenTextures(1, &skybox);

    // Create texture object
    glBindTexture(GL_TEXTURE_CUBE_MAP, skybox);

    // Set texture parameters
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP);

    //glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    // Set environment mode
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    // Specify texture
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image2);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image6);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image5);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image1);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image3);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image4);


    float size = 1.0f;

    list_id = glGenLists(1);
    glNewList(list_id, GL_COMPILE);

    glBegin(GL_QUADS);

        // Near Face
        //glColor3f(1.0f, 0.0f, 0.0f);
        glNormal3f( 0.0f, 0.0f, 1.0f);
        glVertex3f( pos_[0]-size, pos_[1]-size, pos_[2]+size);
        glVertex3f( pos_[0]+size, pos_[1]-size, pos_[2]+size);
        glVertex3f( pos_[0]+size, pos_[1]+size, pos_[2]+size);
        glVertex3f( pos_[0]-size, pos_[1]+size, pos_[2]+size);

        // right Face
        //glColor3f(1.0f, 1.0f, 0.0f);
        glNormal3f( 0.0f, 0.0f, 1.0f);
        glVertex3f( pos_[0]+size, pos_[1]-size, pos_[2]-size);
        glVertex3f( pos_[0]+size, pos_[1]+size, pos_[2]-size);
        glVertex3f( pos_[0]+size, pos_[1]+size, pos_[2]+size);
        glVertex3f( pos_[0]+size, pos_[1]-size, pos_[2]+size);

        // left Face
        //glColor3f(1.0f, 0.0f, 1.0f);
        glNormal3f( 0.0f, 0.0f, 1.0f);
        glVertex3f( pos_[0]-size, pos_[1]-size, pos_[2]+size);
        glVertex3f( pos_[0]-size, pos_[1]+size, pos_[2]+size);
        glVertex3f( pos_[0]-size, pos_[1]+size, pos_[2]-size);
        glVertex3f( pos_[0]-size, pos_[1]-size, pos_[2]-size);

        // back Face
        //glColor3f(0.0f, 1.0f, -1.0f);
        glNormal3f( 0.0f, 0.0f, -1.0f);
        glVertex3f( pos_[0]-size, pos_[1]-size,  pos_[2]-size);
        glVertex3f( pos_[0]-size, pos_[1]+size,  pos_[2]-size);
        glVertex3f( pos_[0]+size, pos_[1]+size,  pos_[2]-size);
        glVertex3f( pos_[0]+size, pos_[1]-size,  pos_[2]-size);

        // upper Face
        //glColor3f(-1.0f,-1.0f, 1.0f);
        glNormal3f( 0.0f, 0.0f, 1.0f);
        glVertex3f( pos_[0]+size, pos_[1]+size, pos_[2]+size);
        glVertex3f( pos_[0]+size, pos_[1]+size, pos_[2]-size);
        glVertex3f( pos_[0]-size, pos_[1]+size, pos_[2]-size);
        glVertex3f( pos_[0]-size, pos_[1]+size, pos_[2]+size);

        // bottom Face
        //glColor3f(-1.0f, -1.0f, 1.0f);
        glNormal3f( 0.0f,0.0f, 1.0f);
        glVertex3f( pos_[0]+size, pos_[1]-size, pos_[2]+size);
        glVertex3f( pos_[0]-size, pos_[1]-size, pos_[2]+size);
        glVertex3f( pos_[0]-size, pos_[1]-size, pos_[2]-size);
        glVertex3f( pos_[0]+size, pos_[1]-size, pos_[2]-size);


//    // Near Face
//    //glColor3f(1.0f, 0.0f, 0.0f);
//    glNormal3f( 0.0f, 0.0f, 1.0f);
//    glVertex3f( -1, -1, 1);
//    glVertex3f( 1, -1, 1);
//    glVertex3f( 1, 1, 1);
//    glVertex3f( -1, 1, 1);

//    // right Face
//    //glColor3f(1.0f, 1.0f, 0.0f);
//    glNormal3f( 0.0f, 0.0f, 1.0f);
//    glVertex3f( 1, -1, -1);
//    glVertex3f( 1, 1, -1);
//    glVertex3f( 1, 1, 1);
//    glVertex3f( 1, -1, 1);

//    // left Face
//    //glColor3f(1.0f, 0.0f, 1.0f);
//    glNormal3f( 0.0f, 0.0f, 1.0f);
//    glVertex3f( -1, -1, 1);
//    glVertex3f( -1, 1, 1);
//    glVertex3f( -1, 1, -1);
//    glVertex3f( -1, -1, -1);

//    // back Face
//    //glColor3f(0.0f, 1.0f, -1.0f);
//    glNormal3f( 0.0f, 0.0f, -1.0f);
//    glVertex3f( -1, -1,  -1);
//    glVertex3f( -1, 1,  -1);
//    glVertex3f( 1, 1,  -1);
//    glVertex3f( 1, -1,  -1);

//    // upper Face
//    //glColor3f(-1.0f,-1.0f, 1.0f);
//    glNormal3f( 0.0f, 0.0f, 1.0f);
//    glVertex3f( 1, 1, 1);
//    glVertex3f( 1, 1, -1);
//    glVertex3f( -1, 1, -1);
//    glVertex3f( -1, 1, 1);

//    // bottom Face
//    //glColor3f(-1.0f, -1.0f, 1.0f);
//    glNormal3f( 0.0f,0.0f, 1.0f);
//    glVertex3f( 1, -1, 1);
//    glVertex3f( -1, -1, 1);
//    glVertex3f( -1, 1, -1);
//    glVertex3f( 1, -1, -1);

    glEnd();
    glEndList();

//    s.enable();

//    texLoc = glGetUniformLocation(s.getProg(), "skybox");
//    glUniform1i(texLoc, 0);

//    s.disable();

}


void SkyBox::moveRight()
{
    glm::vec3 move_r = glm::vec3(1.0f, 0.0f, 0.0f);
    matrix_ = glm::translate(glm::mat4(), move_r);
    pos_ += move_r;
}

void SkyBox::moveLeft()
{
    glm::vec3 move_l = glm::vec3(-1.0f, 0.0f, 0.0f);
    matrix_ = glm::translate(glm::mat4(), move_l);
    pos_ += move_l;

}



void SkyBox::privateRender()
{

    glFrontFace( GL_CW );
    glDisable(GL_DEPTH_TEST);

    s.enable();

    glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_CUBE_MAP);
    glBindTexture(GL_TEXTURE_CUBE_MAP, skybox);

    glCallList(list_id);

    glActiveTexture(GL_TEXTURE0);
    glDisable(GL_TEXTURE_CUBE_MAP);

    s.disable();

    glFrontFace( GL_CCW );
    glEnable(GL_DEPTH_TEST);

   // glTranslatef(glm::vec3 (cam_->getMatrix()[12], cam_->getMatrix()[13], cam_->getMatrix()[14]));
    //glTranslatef(100.0, 100.0, 100.0);
}


void SkyBox::privateUpdate()
{

    //gm->getSkyBox()->setMatrix(gm->getCam()->getMatrix());

}

glm::vec3 SkyBox::getPos(){

    return pos_;
}

void SkyBox::setPos(const glm::vec3 &pos){

   //pos_ = glm::vec3 (cam_->getMatrix()[12], cam_->getMatrix()[13], cam_->getMatrix()[14]);
    //matrix_ = glm::mat4();
    matrix_ = glm::translate(glm::mat4(), pos);
}

GLuint SkyBox::getSkyTex(){

    return skybox;
}


